package vseeding.com.vmap;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.CircleOptions;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiBoundSearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {
    private PoiSearch search;
    private List<PoiInfo> searchResults;
    private LatLng currentPos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        initMapSearch();
        initSearchBtn();
        initNearbyBtn();
        initPosListView();
    }

    private void initMapSearch() {
        search = PoiSearch.newInstance();

        OnGetPoiSearchResultListener poiListener = new OnGetPoiSearchResultListener() {
            public void onGetPoiResult(PoiResult result) {
                if (result.error != SearchResult.ERRORNO.NO_ERROR) {
                    Toast.makeText(getApplicationContext(), "检索失败", Toast.LENGTH_SHORT).show();
                } else {
                    List<String> results = new ArrayList<String>();
                    ListView listView = (ListView) findViewById(R.id.posListView);

                    searchResults = result.getAllPoi();
                    for (int i = 0; i < searchResults.size(); i++) {
                        PoiInfo pi = searchResults.get(i);
                        results.add(pi.address + " " + pi.name);
                        listView.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_expandable_list_item_1, results));
                    }
                }
            }

            public void onGetPoiDetailResult(PoiDetailResult result) {

            }
        };

        search.setOnGetPoiSearchResultListener(poiListener);
    }


    private void initSearchBtn() {
        Button btn = (Button) findViewById(R.id.searchBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText text = (EditText) findViewById(R.id.searchInputText);
                searchPos(text.getText().toString());
            }
        });
    }

    private void initNearbyBtn() {
        Button btn = (Button) findViewById(R.id.nearbyBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPos != null) {
                    MapView mapView = (MapView) findViewById(R.id.mapView);
                    BaiduMap map = mapView.getMap();
                    drawCircle(map, currentPos, 1000);
                    drawMarker(map, new LatLng(currentPos.latitude - 0.001000, currentPos.longitude - 0.001000), R.drawable.icon_nearby);
                    drawMarker(map, new LatLng(currentPos.latitude + 0.001000, currentPos.longitude + 0.001000), R.drawable.icon_nearby);
                    drawMarker(map, new LatLng(currentPos.latitude - 0.003000, currentPos.longitude - 0.003000), R.drawable.icon_nearby);
                    drawMarker(map, new LatLng(currentPos.latitude + 0.003000, currentPos.longitude + 0.003000), R.drawable.icon_nearby);
                }
            }
        });
    }

    private void initPosListView() {
        final ListView listView = (ListView) findViewById(R.id.posListView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showMap(searchResults.get(position));
            }

        });
    }

    private void searchPos(String pos) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(36.629558, 117.130689));
        builder.include(new LatLng(36.66389, 117.15069));
        search.searchInBound((new PoiBoundSearchOption()).keyword(pos).bound(builder.build()));
    }

    private void showMap(PoiInfo poi) {
        MapView mapView = (MapView) findViewById(R.id.mapView);
        mapView.setVisibility(View.VISIBLE);
        BaiduMap map = mapView.getMap();

        map.clear();
        //定义Maker坐标点
        currentPos = new LatLng(poi.location.latitude, poi.location.longitude);

        //定义地图状态
        MapStatus mMapStatus = new MapStatus.Builder().target(currentPos).zoom(15).build();
        //定义MapStatusUpdate对象，以便描述地图状态将要发生的变化

        MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
        //改变地图状态
        map.setMapStatus(mMapStatusUpdate);

        drawMarker(map, currentPos, R.drawable.icon_marka);

    }

    private void drawMarker(BaiduMap map, LatLng point, int icon) {
        BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(icon);
        OverlayOptions option = new MarkerOptions().position(point).icon(bitmap);
        map.addOverlay(option);
    }

    private void drawCircle(BaiduMap map, LatLng center, int radius) {
        CircleOptions opt = new CircleOptions().center(center).radius(radius).fillColor(0xAADAF8FF);
        map.addOverlay(opt);
    }
}
